#placeandroutecpp: C++ place and route libraries

Used internally by the placeandroute python library. Please refer to https://bitbucket.org/StefanoVt/placeandroute for usage details.

## Build instructions

The library can be built with scikit-build (https://github.com/scikit-build/scikit-build ) and CMake. The only dependency not already required
by the placeandroute library are the boost graph libraries (https://www.boost.org/doc/libs/1_67_0/libs/graph/doc/index.html ).
