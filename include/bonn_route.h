#ifndef BONN_ROUTE_H
#define BONN_ROUTE_H
#include<set>
#include<unordered_set>
#include"graph_types.h"
#include"utils.h"

//double calc_score(graph_t &g, std::vector<std::set<vertex_t>> &chains, int capacity);

// increase usage count and weights for the selected nodes
// arguments:
// g : graph
// nodes: unordered_set of nodes to be increased
// capacity: should become a property of g, capacity of the nodes in the graph
// coeff : coefficient for exponential increase of weights
void increase_weights(graph_t &g, std::unordered_set<vertex_t> &nodes, int capacity, double coeff);

//performs bonn routing for the chosen terminals on the selected graph
// arguments:
// g: graph
// out: result parameter, vector of chosen chains
// voi_list: for each terminal set, a set of nodes to be connected. These nodes are already grouped into sets of connected nodes.
// capacity: should become a property of g
// rounds: number of rounds in the steiner trees and randomization phases.
// coeff: coefficient for exponential increase of weights
// path_start, path_end, cluster_help: extra spare nodes that are used by the algorithm
void bonn_route(graph_t &g, std::vector<std::set<vertex_t>> &out, const std::vector<std::set<std::set<vertex_t>>> &voi_list, int capacity, int rounds, double coeff
                , vertex_t path_start, vertex_t path_end, std::vector<vertex_t> &cluster_help);

//todo: proper class for bonn_route
class BonnRoute {

    std::unique_ptr<graph_t> g;
    int capacity;
    std::vector<std::set<vertex_t>> result;
    vertex_t path_start, path_end;
    std::vector<vertex_t> cluster_help;
private:
    void reset();

public:
    BonnRoute(std::unique_ptr<graph_t> g, int capacity);

    std::vector<std::set<vertex_t>> & run(std::vector<std::set<vertex_t>> &terminals, int effort);
};


#endif // BONN_ROUTE_H
