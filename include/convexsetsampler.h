#ifndef CONVEXSETSAMPLER_H
#define CONVEXSETSAMPLER_H
#include<unordered_map>
#include<random>
#include"utils.h"

namespace ConvexSetSamplerNS {
using namespace std; // inside a wrapper namespace to avoid clashes/pollution

// generic multiset that can be sampled, keeps also track of the most common element
// a multiset can be considered as a convex sum of elements, with
// where each element has coefficient count(x)/total()
// in this case, multiset sampling is equivalent to weighted sampling
template<typename T>
class ConvexSetSampler
{
    unordered_map<T,unsigned int> db;
    typedef typename unordered_map<T, unsigned int>::iterator iterator_t;
    iterator_t most_common;
    unsigned int count = 0;
public:
    ConvexSetSampler(): most_common(db.end()){}

    // add an element to the multiset
    iterator_t add(T &el){
        pair<iterator_t, bool> search = db.emplace(el, 0);
        iterator_t ret = search.first;
        ret->second++;
        count++;

        // todo: iterators can be invalidated rarely (by the previous emplace), might fail
        if (most_common == db.end() || most_common->second < ret->second){
            most_common = ret;
        }

        return ret;
    }

    // get the most common element, as an iterator/reference
    iterator_t get_most_common(){
        return  most_common;
    }

    // sample an element from the multiset
    iterator_t sample(){
        iterator_t ret = db.begin();
        uniform_int_distribution<int> rand(1, count);
        int select = rand(generator);
        while (select > ret->second){
            select -= ret->second;
            ret++;
        }
        return ret;
    }
};
}

using ConvexSetSamplerNS::ConvexSetSampler;
#endif // CONVEXSETSAMPLER_H
