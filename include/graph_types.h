#ifndef GRAPH_TYPES_H
#define GRAPH_TYPES_H
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>

// each vertex tracks its usage
// note: on special nodes used for steiner trees calculation this parameter is used for a diff purpose, see steiner_trees.cpp for details
struct VertexProperty {
    int usage;

    VertexProperty(int u) : usage(u) {}
    VertexProperty() : usage(0) {}
};

// type for graph, use vectors for storage (algo is heavy on out_edges and in_edges iterations, no memory concerns), an int property for vertices (usage)
// and double weight for edges.
typedef boost::adjacency_list < boost::vecS, boost::vecS, boost::bidirectionalS,
VertexProperty, boost::property < boost::edge_weight_t, double > > graph_t;

// using vector storage is just an int, changing storage turns it into a void* breaking a lot of stuff
typedef boost::graph_traits < graph_t >::vertex_descriptor vertex_t;


#endif // GRAPH_TYPES_H
