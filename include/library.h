#ifndef PLACEANDROUTECPP_LIBRARY_H
#define PLACEANDROUTECPP_LIBRARY_H

#include "graph_types.h"
#include <set>
#include <vector>
#include <boost/python.hpp>




// main class called by python. todo: move parts to BonnRoute class and turn it into thin wrapper
class GraphClass {
    graph_t graphobj;
    std::vector<std::set<std::set<vertex_t>>> terminals;
    vertex_t path_start;
    vertex_t path_end ;
    std::vector<vertex_t> cluster_help;
public:
    std::vector<std::set<vertex_t>> chains;
    double coeff = 3.0;
    int capacity = 4;

    //graph construction
    //low level interface, conversion from networkx object is handled by python code
    GraphClass();
    vertex_t add_vertex_m();
    void add_edge_m(vertex_t a, vertex_t b);

    void increase_weights_m(boost::python::object nodes);

    // helper to pass complex data structures from python to cpp
    void add_terminals(boost::python::object voi_list);
    // run the main algo
    void bonn_route_m(int rounds);

    // reset the class state, allows reuse
    void reset();

    // returns algo result to python land
    boost::python::list read_chain(int index);

};

#endif
