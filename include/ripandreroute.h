#ifndef RIPANDREROUTE_H
#define RIPANDREROUTE_H
#include"graph_types.h"
#include<unordered_map>
// WIP

// a constraint is represented as a list of list of vars
typedef std::vector<std::vector<size_t>> constraint_t;

namespace std
{
    template<> struct hash<constraint_t>
    {

        size_t operator()(constraint_t const& s) const noexcept
        {
            return reinterpret_cast<size_t>(&s);
        }
    };
}

class RipAndReroute
{
    graph_t &graph;
    std::vector<std::set<vertex_t>> &chains;
    std::vector<std::vector<vertex_t>> &choices;
    std::unordered_map<constraint_t, std::vector<vertex_t>> &constraints;

public:
    RipAndReroute(graph_t &graph, std::vector<std::vector<vertex_t>> &choices, std::vector<std::set<vertex_t>> &chains, std::unordered_map<constraint_t, std::vector<vertex_t>> &constraints);

    void run(int effort);
    const constraint_t &find_worst();
    void rip(const constraint_t &c);
    std::vector<vertex_t> & find_best_place(const constraint_t &);
};

#endif // RIPANDREROUTE_H
