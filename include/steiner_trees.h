#ifndef STEINER_TREES_H
#define STEINER_TREES_H
#include "graph_types.h"
#include <set>
#include<unordered_set>

//avoid allocating temporary sets and use an iterator over vertices instead
typedef std::pair<graph_t::adjacency_iterator, graph_t::adjacency_iterator> vertex_slice_t;

// returns a single seiner tree from the graph
// args
// g: the main graph
// clusters: clusters of vertices that have to be connected by the steiner tree
// start_node, end_node, cluster_help: special nodes used by the algorithm
vertex_slice_t fast_steiner_tree(graph_t &g, const std::set<std::set<vertex_t>> &clusters,
                       vertex_t start_node, vertex_t end_node, std::vector<vertex_t> &cluster_help);

#endif // STEINER_TREES_H
