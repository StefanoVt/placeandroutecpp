#ifndef UTILS_H
#define UTILS_H
#include <algorithm>
#include <random>
#include <set>
#include<iostream>
#include "graph_types.h"
// exp is safe up to about 706, worst case scenario is the sum  of thousands of HUGE_VAL terms, bound lower at 600
static inline double bounded_exp(double pow){ return exp(std::min(600.0, pow));}

// global RNG
extern std::default_random_engine generator;

template<typename A, typename B>
inline static std::pair<B,A> flip_pair(const std::pair<A,B> &p)
{
    return std::pair<B,A>(p.second, p.first);
}

// obsolete, see convexsetsampler.h
template<typename T>
const T& sample_multiset(const std::multiset<T> &the_set){
    std::uniform_int_distribution<unsigned long> distribution(1, the_set.size());
    unsigned long selected = distribution(generator) - 1;
    auto it = the_set.cbegin();
    assert(selected < the_set.size());
    std::advance(it, selected);
    assert(the_set.count(*it) > 0);
    return *it;
}

//debug function
template<typename T>
void print_set(const std::set<T> &set)
{
    std::cout << "set[";
    for (T &el : set)
    {
        std::cout << el << ", ";
    }
    std::cout << "]";
}

template<typename T>
void print_multiset(const std::multiset<std::set<T>> &mset)
{
    std::cout << "multiset[";
    for (auto it = mset.begin(); it!= mset.end(); std::advance(it, mset.count(*it)))
    {
        print_set(*it);
        std::cout << ":" << mset.count(*it);
    }
    std::cout << "]";
}



#endif // UTILS_H
