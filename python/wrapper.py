import networkx as nx
import placeandroutecpp.libplaceandroutecpp as cpp

class MinMaxRouter:
    """Wrapper class, feature equivalent to placeandroute.bonnroute.MinMaxRouter"""
    def __init__(self, nxGraph, **_):
        """Initialize object from a networkx graph"""
        _graph = cpp.BGLGraph()
        nodemap = dict()
        revmap = dict()
        capacity = None

        for node, ndata in nxGraph.nodes(data=True):
            mn = _graph.add_vertex()

            #maps from nx nodes to bgl nodes and back
            nodemap[node] = mn
            revmap[mn] = node

            # get the capacity from the nx graph, for now it must be equal for all nodes
            if capacity is None:
                capacity = ndata["capacity"]
            else:
                if ndata["capacity"] != capacity:
                    raise NotImplementedError("Different capacities not supported in cpp version")

        for node1, node2 in nxGraph.edges:
            _graph.add_edge(nodemap[node1], nodemap[node2])
            _graph.add_edge(nodemap[node2], nodemap[node1])

        self._graph = _graph
        self.nx_to_bgl = nodemap
        self.bgl_to_nx = revmap
        self.capacity = capacity
        self.orig_graph = nxGraph
        _graph.capacity = capacity
        self.result = None


    def increase_weights(self, nodes):
        self._graph.increase_weights(set(map(self.nx_to_bgl.get, nodes)))


    def run(self, terminals, epsilon, effort):
        rounds = effort

        # reset cpp class
        self._graph.reset()
        self._graph.coeff = epsilon

        # initialize terminals
        term_ids = list(terminals.keys())
        for id in term_ids:
            vois = self.orig_graph.subgraph(terminals[id])
            voi_clusters = nx.connected_components(vois)
            self._graph.add_terminals([[self.nx_to_bgl[x] for x in xx] for xx in voi_clusters])

        self._graph.bonn_route(rounds)

        # read back chains
        chains = dict()
        for index, key in enumerate(term_ids):
            vals_nx = set(map(self.bgl_to_nx.get, self._graph.read_chain(index)))
            assert nx.is_connected(self.orig_graph.subgraph(vals_nx)), list(nx.connected_components(self.orig_graph.subgraph(vals_nx)))
            chains[key] = vals_nx
        self.result = chains
        return self.result

    def __getstate__(self):
        # required for multiprocessing, we care only about the result when moving objects between processes
        return (self.result,)

    def __setstate__(self, p):
        # required for multiprocessing, we care only about the result when moving objects between processes
        self.result = p[0]

