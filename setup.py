#!/usr/bin/env python3
try:
    from skbuild import setup
except ModuleNotFoundError:
    from setuptools import setup


setup(name="placeandroutecpp",
    version="0.0.0",
packages=['placeandroutecpp'],
install_requires=["scikit-build"]
    )