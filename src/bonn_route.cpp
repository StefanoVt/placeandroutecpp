#include <iostream>
#include <random>
#include <map>
#include <algorithm>
#include <chrono>
#include <functional>
#include <set>
#include <boost/property_map/property_map.hpp>
#include "library.h"
#include "steiner_trees.h"
#include "bonn_route.h"
#include "convexsetsampler.h"

using namespace std;
using namespace boost;

namespace std {
template<>
struct hash<std::unordered_set<vertex_t>>{
    size_t operator() (const std::unordered_set<vertex_t> &ref) const{
        size_t ret=0;
        for(vertex_t elem : ref){
            ret += reinterpret_cast<size_t>(elem);
        }
        return ret;
    }
};
}

void increase_weights_node(graph_t &g, vertex_t &node, property_map<graph_t, edge_weight_t>::type weightmap,  int capacity,  double coeff)
{
    g[node].usage += 1;
    double ratio = (static_cast<double>(g[node].usage) / capacity);
    double edge_weight = bounded_exp(max(0.0, coeff*ratio));
    for (auto it = in_edges(node,g); it.first != it.second; it.first++)
    {
        auto const&i = *(it.first);
        //graph_t::edge_descriptor opposite = edge(target(i,g), source(i,g), g).first;
        weightmap[i] = edge_weight;
    }
}

void increase_weights(graph_t &g, std::unordered_set<vertex_t> &nodes,  int capacity,  double coeff)
{
    property_map<graph_t, edge_weight_t>::type weightmap = get(edge_weight, g);
    for (vertex_t node : nodes)
    {
        increase_weights_node(g, node, weightmap, capacity, coeff);
    }

}

class OverloadCount
{
    map<vertex_t, int> count;
    graph_t &g;
    int capacity;
    double coeff;
public:
    OverloadCount (graph_t &gp, vector<std::reference_wrapper<const std::unordered_set<vertex_t> >> &chains, int capacit)
        : g(gp), capacity(capacit)
    {
        coeff = log(static_cast<double>(num_vertices(g))*capacity);

        for (auto it = chains.cbegin(); it!= chains.cend(); it++)
        {
            add(*it);
        }
    }
    void peel(const std::unordered_set<vertex_t> &chain)
    {
        for (auto v : chain){
            count[v] -= 1;
        };
    }

    void add(const std::unordered_set<vertex_t> &chain)
    {
        for (auto itt= chain.cbegin(); itt != chain.cend(); itt++)
        {
            if (count.count(*itt) == 0)
            {
                count[*itt] = 0;
            }
            count[*itt] +=1;
        }
    }

    double calc_score()
    {
        double total = 1;
        for (auto items : count)
        {
            total += bounded_exp(coeff* max(0, (items.second) - capacity)) - 1;
        }
        return total;
    }
};


void bonn_route(graph_t &g, vector<set<vertex_t>> &out, const vector<set<set<vertex_t>>> &voi_list, int capacity, int rounds, double coeff
                , vertex_t path_start, vertex_t path_end, std::vector<vertex_t> &cluster_help)
{
    //todo: use unique_ptr to avoid moving, speed up tree comparison
    vector<ConvexSetSampler<std::unordered_set<vertex_t>>> tree_counter(voi_list.size());
    property_map<graph_t, edge_weight_t>::type weightmap = get(edge_weight, g);


    //find convex trees
#ifndef NDEBUG
    cout << "steiner tree rounds..." << flush;
#endif
    for (int round=1; round <= rounds; round++)
    {
#ifndef NDEBUG
//                cout <<  " " << round << flush;
#endif
        for (size_t i=0; i< voi_list.size(); i++)
        {
            const set<set<vertex_t>> &voi_clusters = voi_list[i];
            std::unordered_set<vertex_t> tree;


            auto tree_iter = fast_steiner_tree(g, voi_clusters, path_start, path_end, cluster_help);
            //print_set(tree);
            //cout << endl;
            /*for (set<vertex_t> cluster: voi_clusters){
                for (vertex_t cnode : cluster){
                    assert(tree.count(cnode) > 0);
                }
            }*/
            for(; tree_iter.first != tree_iter.second; tree_iter.first++){
                vertex_t node = *(tree_iter.first);
                tree.insert(node);
                increase_weights_node(g, node, weightmap, capacity, coeff);
            }
            tree_counter[i].add(tree);
            //increase_weights(g, tree, capacity, coeff);
        }
    }
    /*
    for (int i = 0; i < voi_list.size(); i++)
    {
            for (set<vertex_t> cluster: voi_list[i]){
                for (vertex_t cnode : cluster){
                    for (set<vertex_t> chain : tree_counter[i]){
                         assert(chain.count(cnode) > 0);
                    }
                }
            }

    }*/

    //randomization
    vector<std::reference_wrapper<const std::unordered_set<vertex_t>>> chains;

    for (size_t i = 0; i < tree_counter.size(); i++)
    {

        //print_multiset(tree_dist);cout << endl;
        ConvexSetSampler<std::unordered_set<vertex_t>> &tree_dist = tree_counter[i];
//        std::reference_wrapper<const set<vertex_t>> reference(sample_multiset(tree_dist));
        std::reference_wrapper<const std::unordered_set<vertex_t>> reference(tree_dist.get_most_common()->first);

        /*for (set<vertex_t> cluster: voi_list[i]){
            for (vertex_t cnode : cluster){
                assert(reference.get().count(cnode) > 0);
            }
        }*/
        chains.push_back(reference);
        assert(chains[i].get() == reference.get());
    }
    assert(chains.size() == tree_counter.size());


    /*for (int i = 0; i < voi_list.size(); i++)
    {
            for (set<vertex_t> cluster: voi_list[i]){
                for (vertex_t cnode : cluster){
                    assert(chains[i].get().count(cnode) > 0);
                }
            }

    }*/
#ifndef NDEBUG
    cout << " end" << endl <<  "randomization rounds..." <<  flush;
#endif
    OverloadCount ov_count(g,chains, capacity);

    double best_score = ov_count.calc_score();
    for (int round=1; round <= rounds; round++){
        //anneal here
#ifndef NDEBUG
               // cout <<  " " << round << flush;
#endif
        for (size_t rand_el=0; rand_el < tree_counter.size(); rand_el++){
            if (best_score <= 1.001) break;
            std::reference_wrapper<const std::unordered_set<vertex_t>> old_ptr = chains[rand_el];
            /*for (set<vertex_t> cluster: voi_list[rand_el]){
                for (vertex_t cnode : cluster){
                    assert(chains[rand_el].get().count(cnode) > 0);
                }
            }*/
            std::reference_wrapper<const std::unordered_set<vertex_t>> new_ptr =  std::reference_wrapper<const std::unordered_set<vertex_t>>(tree_counter[rand_el].sample()->first);
            if (old_ptr.get() == new_ptr.get()) continue;

            chains[rand_el] = new_ptr;
            ov_count.peel(old_ptr);
            ov_count.add(chains[rand_el]);
            /*for (set<vertex_t> cluster: voi_list[rand_el]){
                for (vertex_t cnode : cluster){
                    assert(chains[rand_el].get().count(cnode) > 0);
                }
            }*/
            double score = ov_count.calc_score();
            bernoulli_distribution jump_up_with_prob(0.1 * best_score/(score * round));
            if (score < best_score ||  jump_up_with_prob(generator) ){
                best_score = score;
            } else {
                ov_count.peel(chains[rand_el]);
                ov_count.add(old_ptr);
                chains[rand_el] = old_ptr;
            }
            /*
            for (set<vertex_t> cluster: voi_list[rand_el]){
                for (vertex_t cnode : cluster){
                    assert(chains[rand_el].get().count(cnode) > 0);
                }
            }*/
        }
    }



    out.clear();
    for (auto chain_ptr : chains)
    {
        //print_set(chain_ptr); cout << endl;
        auto &convert_me = chain_ptr.get();
        out.emplace_back(convert_me.begin(), convert_me.end());
    }

    /*
    for (int i = 0; i < voi_list.size(); i++)
    {
            for (set<vertex_t> cluster: voi_list[i]){
                for (vertex_t cnode : cluster){
                    assert(out[i].count(cnode) > 0);
                }
            }

    }*/
#ifndef NDEBUG
    cout << "  end" << endl;
#endif
}

