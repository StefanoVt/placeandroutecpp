#include "library.h"
#include "steiner_trees.h"
#include <iostream>
#include <random>
#include <map>
#include <algorithm>
#include <chrono>
#include <functional>
#include <boost/property_map/property_map.hpp>
#include "bonn_route.h"

using namespace boost;
using namespace std;





BOOST_PYTHON_MODULE(libplaceandroutecpp)
{
    using namespace boost::python;
    class_<GraphClass>("BGLGraph")
        .def_readwrite("capacity", &GraphClass::capacity)
        .def_readwrite("coeff", &GraphClass::coeff)
        .def_readwrite("chains", &GraphClass::chains)
        .def("add_vertex", &GraphClass::add_vertex_m)
        .def("add_edge", &GraphClass::add_edge_m)
        .def("increase_weights", &GraphClass::increase_weights_m)
        .def("add_terminals", &GraphClass::add_terminals)
        .def("bonn_route", &GraphClass::bonn_route_m)
        .def("reset", &GraphClass::reset)
        .def("read_chain", &GraphClass::read_chain);
}//*/





vertex_t GraphClass::add_vertex_m(){
    return boost::add_vertex(0, graphobj);
}

void GraphClass::add_edge_m(vertex_t a, vertex_t b)
{
    if (!edge(a, b, graphobj).second) boost::add_edge(a,b, 1, graphobj);
}

void GraphClass::increase_weights_m(python::object nodes){

    python::stl_input_iterator<vertex_t> begin(nodes), end;
    std::unordered_set<vertex_t> converted;
    converted.insert(begin, end);
    //print_set(converted);
    //cout << endl;
    increase_weights(graphobj, converted, capacity, coeff);
}

void GraphClass::add_terminals(boost::python::object voi_list)
{
    python::stl_input_iterator<python::object> it_cluster(voi_list), end;
    set<set<vertex_t>> term_clusters;
    for(; it_cluster != end; it_cluster++)
    {
        python::stl_input_iterator<vertex_t> it_nodes(*it_cluster), node_end;
        std::unordered_set<vertex_t> cluster_un(it_nodes, node_end);
        set<vertex_t> cluster(cluster_un.begin(), cluster_un.end());
        increase_weights(graphobj, cluster_un, capacity, coeff);
        term_clusters.insert(cluster);
    }
    //print_set(converted); cout << endl;
    terminals.push_back(term_clusters);
}

void GraphClass::bonn_route_m( int rounds)
{
    //cout << "bonn_route_m" << endl;
    bonn_route(graphobj, chains, terminals, capacity, rounds, coeff, path_start, path_end, cluster_help);
    /*
    assert(terminals.size() == chains.size());
    for (size_t i = 0; i < chains.size(); i++)
    {
            for (set<vertex_t> cluster: terminals[i]){
                for (vertex_t cnode : cluster){
                    assert(chains[i].count(cnode) > 0);
                }
            }

    }*/
}

boost::python::list GraphClass::read_chain(int index)
{
    python::list ret;
    assert(index < chains.size());
    for (auto chainel : chains[index]){
        ret.append(chainel);
    }
    return ret;
}

GraphClass::GraphClass() : graphobj(), chains()
{
    generator.seed(std::chrono::system_clock::now().time_since_epoch().count());
    path_start = add_vertex(graphobj);
    path_end = add_vertex(graphobj);
}

void GraphClass::reset()
{
    chains.clear();
    terminals.clear();
    auto weights = get(edge_weight, graphobj);
    for (auto it=vertices(graphobj); it.first != it.second; it.first++)
    {
        graphobj[*(it.first)].usage = 0;
    }
    for(auto it = edges(graphobj); it.first != it.second; it.first++)
    {
        weights[*it.first] = 1;
    }
}
