#include "ripandreroute.h"
//not yet ready, unused
using namespace std;

RipAndReroute::RipAndReroute(graph_t &graph, vector<vector<vertex_t>> &choices, vector<set<vertex_t>> &chains, std::unordered_map<constraint_t, std::vector<vertex_t>> &constraints)
    : graph(graph),  chains(chains), choices(choices), constraints(constraints)
{

}

void RipAndReroute::run(int effort)
{
    const constraint_t &constraint = find_worst();
    rip(constraint);
    vector<vertex_t> new_place = find_best_place(constraint);
    //reroute();
}

const constraint_t &RipAndReroute::find_worst()
{
    double best_score = 0;
    const constraint_t *best;
    for (auto it : constraints){
        double score = 0;
        for (vertex_t mapped_node : it.second){
            score += 1; // XXX
        }
        if (score >= best_score){
            best = & it.first;
        }
    }
    return *best;
}

void RipAndReroute::rip(const constraint_t &c)
{
    constraints.erase(c);
}

std::vector<vertex_t> &RipAndReroute::find_best_place(const constraint_t &c)
{
    set<vertex_t> all_vertices;
    all_vertices.insert(c[0].begin(), c[0].end());
    all_vertices.insert(c[1].begin(), c[1].end());
    //calc all disrances
    unordered_map<vertex_t, unordered_map<vertex_t, double>> distances;
    for (auto it : choices){

    }
}
