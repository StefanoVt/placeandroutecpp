#include <steiner_trees.h>
#include <vector>
#include <boost/graph/dijkstra_shortest_paths.hpp>
//#include <boost/graph/filtered_graph.hpp>
#include<iostream>
#include<boost/graph/astar_search.hpp>
#include<map>
#include<functional>
#include "utils.h"

using namespace std;
using namespace boost;
using std::unordered_set;


struct found_goal {}; // exception for termination

// when edgelist is not a set, check here
inline static graph_t::edge_descriptor maybe_add_edge(vertex_t from, vertex_t to, double weight, graph_t &g)
{
    auto the_edge = edge(from, to, g);
    if (!the_edge.second) return add_edge(from, to, weight, g).first;
    else return the_edge.first;
}

// visitor that terminates when we find the goal
class astar_goal_visitor : public boost::default_astar_visitor
{
public:
    astar_goal_visitor(vertex_t goal) : m_goal(goal) {}
    template <class Graph>
    void examine_vertex(vertex_t u, Graph& g) {
        if(u == m_goal)
            throw found_goal();
    }
private:
    vertex_t m_goal;
};


void fast_steiner_tree_old(set<vertex_t> &ret, graph_t &g, const set<set<vertex_t>> &clusters, vertex_t path_start, vertex_t path_end)
{
    ret.clear();
    clear_vertex(path_start, g);

    auto cluster_iter = clusters.cbegin();

    for (auto it : *cluster_iter) {
        maybe_add_edge(path_start, it, 0, g);
        ret.insert(it);
    }
    cluster_iter++;

    while (cluster_iter != clusters.cend()){
        const set<vertex_t> &clust = *cluster_iter;

        clear_vertex(path_end, g);
        for (auto it : clust) {
            maybe_add_edge(it, path_end, 0, g);
            set<vertex_t> linkme;
            for (auto neigh = out_edges(it, g); neigh.first != neigh.second; neigh.first++)
            {
                vertex_t nn = target(*neigh.first, g);
                if (nn == path_start) continue;
                linkme.insert(nn);

            }
            for (auto v : linkme) maybe_add_edge(v, path_end, 0, g);
        }
        std::vector<vertex_t> p(num_vertices(g));
        std::vector<double> d(num_vertices(g));
        auto indices  = get(boost::vertex_index, g);

#ifndef NDEBUG

        property_map<graph_t, edge_weight_t>::type weightmap = get(edge_weight, g);

        for (auto it = edges(g); it.first != it.second; it.first++)
        {
            if (weightmap[*it.first] <0) cout <<"(" << weightmap[*it.first] << ")wrong weight" << endl;
            assert(weightmap[*it.first] >= 0.0);
        }
#endif
        try{
            astar_search(g, path_start, astar_heuristic<graph_t, double>(),
                         predecessor_map(boost::make_iterator_property_map(p.begin(), indices)).
                         distance_map(boost::make_iterator_property_map(d.begin(), indices)).
                         visitor(astar_goal_visitor(path_end)));
        } catch(found_goal _) {}
        vertex_t addme = p[indices[path_end]];
        while (addme != path_start)
        {
            assert(addme != path_end);
            maybe_add_edge(path_start, addme, 0, g);
            ret.insert(addme);
            addme = p[indices[addme]];
        }
        for (auto it : clust) {
            maybe_add_edge(path_start, it, 0, g);
            ret.insert(it);
        }


        cluster_iter++;
    }

}

vertex_slice_t fast_steiner_tree(graph_t &g, const set<set<vertex_t>> &clusters
                       , vertex_t path_start, vertex_t path_end, std::vector<vertex_t> &cluster_help)
{
    //ret.clear();
    clear_vertex(path_start, g);
    clear_vertex(path_end, g);
    for (auto cnode: cluster_help){
        clear_vertex(cnode, g);
    }
    if (clusters.size() > cluster_help.size()){
        for (auto i=cluster_help.size(); i <= clusters.size(); i++){
            vertex_t clust_v = add_vertex(g);
            //use usage slot for saving position in cluster_help vector
            g[clust_v].usage = static_cast<int>(cluster_help.size());
            cluster_help.push_back(clust_v);
        }
    }
    size_t used_clusters = 0;



    std::uniform_int_distribution<int> distribution(0, static_cast<int>(clusters.size())-1);
    int random_start = distribution(generator);


    //map<vertex_t, std::reference_wrapper<const set<vertex_t>>> cluster_map;
    vector<const set<vertex_t>*> cluster_map(cluster_help.size());
    //map<vertex_t, vertex_t> cluster_mapn;
    int to_connect = 0;

    for (auto cluster_iter = clusters.cbegin(); cluster_iter != clusters.cend();    cluster_iter++)
    {
        if (random_start-- == 0){
            for (auto it : *cluster_iter) {
                add_edge(path_start, it, 0, g);
                //ret.insert(it);
            }
        }
        else{
            const set<vertex_t> &clust = *cluster_iter;
            vertex_t cluster_node = cluster_help[used_clusters++];
            vector<vertex_t> buffer(num_vertices(g));
            for (auto it : clust) {
                //set<vertex_t> linkme;
                buffer.clear();
                for (auto neigh = in_edges(it, g); neigh.first != neigh.second; neigh.first++)
                {
                    vertex_t nn = source(*neigh.first, g);
                    //linkme.insert(nn);
                    //if (nn == path_end) continue;
                    for (auto cn: cluster_help) assert(cn != nn);
                    assert(nn != path_start);
                    //for (auto cn: cluster_help) if (cn == nn) continue;
                    //maybe_add_edge(nn, cluster_node, 0, g);
                    buffer.push_back(nn);
                }
                for(vertex_t nn : buffer) maybe_add_edge(nn, cluster_node,0,g);
                maybe_add_edge(it, cluster_node, 0, g);
                add_edge(cluster_node, path_end, 0, g);
                //for (auto v : linkme) maybe_add_edge(path_start, v, 0, g);
                //cluster_mapn.emplace(it, cluster_node);
                cluster_map[g[cluster_node].usage] = &clust;


            }
            to_connect++;
        }
    }
    while (to_connect > 0)
    {
        std::vector<vertex_t> p(num_vertices(g));
        std::vector<double> d(num_vertices(g));
        auto indices  = get(boost::vertex_index, g);

#ifndef NDEBUG

        property_map<graph_t, edge_weight_t>::type weightmap = get(edge_weight, g);

        for (auto it = edges(g); it.first != it.second; it.first++)
        {
            if (weightmap[*it.first] <0) cout <<"(" << weightmap[*it.first] << ")wrong weight" << endl;
            assert(weightmap[*it.first] >= 0.0);
        }
#endif
        try{
            astar_search(g, path_start, astar_heuristic<graph_t, double>(),
                         predecessor_map(boost::make_iterator_property_map(p.begin(), indices)).
                         distance_map(boost::make_iterator_property_map(d.begin(), indices)).
                         visitor(astar_goal_visitor(path_end)));
        } catch(found_goal _) {}
        vertex_t addme = p[indices[path_end]];


        //assert(cluster_map.count(addme) > 0);
        const set<vertex_t> &cluster_reference = *(cluster_map[g[addme].usage]);
        clear_vertex(addme,  g);
        addme = p[indices[addme]];

        for (auto it : cluster_reference) {
            add_edge(path_start, it, 0, g);
            //ret.insert(it);
        }

        while (addme != path_start)
        {
            assert(addme != path_end);
            maybe_add_edge(path_start, addme, 0, g);
            //ret.insert(addme);
            addme = p[indices[addme]];
        }



        to_connect--;

    }
    //for (auto cn: cluster_help) assert(out_degree(cn, g) == 0);
    auto retnodes = adjacent_vertices(path_start, g);
    //ret.insert(retnodes.first,retnodes.second);
    return retnodes;
}
